from tkinter import *
import datetime

class Quiz(Frame):
    def __init__(self, master):
        Frame.__init__(self, master)
        self.master = master
        self.flag = True
        self.count = 0
        self.percent = 0
        self.ansDict = {}
        self.displayRowCount = {}
        self.rowCount = 1
        self.q1a = IntVar()
        self.q1b = IntVar()
        self.q2a = IntVar()
        self.q2b = IntVar()
        self.q3a = IntVar()
        self.q3b = IntVar()
        self.q3c = IntVar()
        self.q3d = IntVar()
        self.q4a = IntVar()
        self.q4b = IntVar()
        self.q5a = IntVar()
        self.q5b = IntVar()
        self.q5c = IntVar()
        self.q5d = IntVar()
        self.q6a = IntVar()
        self.q6b = IntVar()
        self.q7a = IntVar()
        self.q7b = IntVar()
        self.q8a = IntVar()
        self.q8b = IntVar()
        self.q9a = IntVar()
        self.q9b = IntVar()
        self.q10a = IntVar()
        self.q10b = IntVar()
                       
        self.initUIquiz2()
    
    def quizFramework(self, qno):
        titleText = "Quiz " + str(qno)
        self.master.title(titleText)
        self.frameOne = Frame(self.master)
        self.frameOne.grid(row = 0, column = 0)
        self.mainlbl = Label(self.frameOne, text = titleText, background = "white", foreground = "blue", font = "Times 20", relief = "groove")
        self.mainlbl.grid(row = 0, column = 0)
        self.space = Label(self.frameOne, text = " ")
        self.space.grid(row = 1, column = 0)
        self.space1 = Label(self.frameOne, text = " ")
        self.space1.grid(row = 2, column = 0)

        # qframe is the name of the frame that will contain the canvas and another frame which will have all the questions.
        self.qframe = Frame(self.master)
        self.qframe.grid(row = 1, column = 0)
        self.canvas = Canvas(self.qframe)
        self.listFrame = Frame(self.canvas) 
        ''' Two things need to be done to connect a vertical scrollbar to a widget:
            1.  Set the scrollbar's command to the yview method of the widget (canvas).
            2.  Set the widget's (canvas in this case) yscrollcommand callbacks to the set method of the scrollbar.'''
        self.scrolly = Scrollbar(self.master, orient = VERTICAL, command = self.canvas.yview)
        self.scrolly.grid(row = 1, column = 1, sticky = 'nsew')
        self.canvas['yscrollcommand'] = self.scrolly.set 
        
        self.scrollx = Scrollbar(self.master, orient = HORIZONTAL, command = self.canvas.xview)
        self.scrollx.grid(row = 20, column = 0, sticky = 'ew')
        self.canvas['xscrollcommand'] = self.scrollx.set  

        # The create_window method places a tkinter widget on the canvas (in this case listFrame).
        self.canvas.create_window((0, 0), window = self.listFrame, anchor = 'nw')
        self.canvas.pack(side = "left")
        self.listFrame.bind("<Configure>", self.questionScrollFunction)
                
    def initUIquiz1(self):
        self.quizFramework(1)
        # Add the questions to listFrame which will be scrollable as needed.
        self.q1Lbl = Label(self.listFrame, text = "1. Python is an OO Language")
        self.q1Lbl.grid(row=1, column=0, padx=(10,0), sticky = "w")
        self.cb1 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q1a)
        self.cb1.grid(row=2, column=0, padx=(10,0), sticky = "w")
        self.cb2 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q1b)
        self.cb2.grid(row=3, column=0, padx=(10,0), sticky = "w")
        self.s1lbl = Label(self.listFrame, text = " ")
        self.s1lbl.grid(row = 4, column = 0)

        self.q2lbl = Label(self.listFrame, text = "2. Dave Brady is an awesome Python programmer")
        self.q2lbl.grid(row=5, column=0, padx=(10,0), sticky = "w")
        self.cb3 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q2a)
        self.cb3.grid(row=6, column=0, padx=(10,0), sticky = "w")
        self.cb4 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q2b)
        self.cb4.grid(row=7, column=0, padx=(10,0), sticky = "w")
        self.s2lbl = Label(self.listFrame, text = "   ")
        self.s2lbl.grid(row = 8, column = 0)

        self.q3lbl = Label(self.listFrame, text = "3. Check all responses that are true about Python")
        self.q3lbl.grid(row=9, column=0, padx=(10,0), sticky = "w")
        self.cb5 = Checkbutton(self.listFrame, text = "a.  Python is a fully compiled language", variable = self.q3a)
        self.cb5.grid(row=10, column=0, padx=(10,0), sticky = "w")
        self.cb6 = Checkbutton(self.listFrame, text = "b.  Python code is executed by an interpreter", variable = self.q3b)
        self.cb6.grid(row=10, column=1, padx=(10,0), sticky = "w")
        self.cb7 = Checkbutton(self.listFrame, text = "c.  Python is a multi-platform language", variable = self.q3c)
        self.cb7.grid(row=11, column=0, padx=(10,0), sticky = "w")
        self.cb8 = Checkbutton(self.listFrame, text = "d.  Python is faster than C or C++", variable = self.q3d)
        self.cb8.grid(row=11, column=1, padx=(10,0), sticky = "w")
        self.s3lbl = Label(self.listFrame, text = "   ")
        self.s3lbl.grid(row = 12, column = 0)

        self.q4lbl = Label(self.listFrame, text = "4. IDLE is Python's built-in development environment")
        self.q4lbl.grid(row = 14, column = 0, sticky = "w")
        self.cb9 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q4a)
        self.cb9.grid(row = 15, column = 0, sticky = "w")
        self.cb10 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q4b)
        self.cb10.grid(row = 16, column = 0, sticky = "w")
        self.s4lbl = Label(self.listFrame, text = "   ")
        self.s4lbl.grid(row = 17, column = 0)

        self.q5lbl = Label(self.listFrame, text = "5. Check all true responses ")
        self.q5lbl.grid(row = 18, column = 0, sticky = "w")
        self.cb11 = Checkbutton(self.listFrame, text = "a.  Python has two number types (Floats and Integers)", variable = self.q5a)
        self.cb11.grid(row = 19, column = 0, sticky = "w")
        self.cb12 = Checkbutton(self.listFrame, text = "b.  A Python List is immutable (can't be changed)", variable = self.q5b)
        self.cb12.grid(row = 19, column = 1, sticky = "w")
        self.cb13 = Checkbutton(self.listFrame, text = "c.  A Python Tuple is immutable", variable = self.q5c)
        self.cb13.grid(row = 20, column = 0, sticky = "w")
        self.cb14 = Checkbutton(self.listFrame, text = "d.  Strings act like a List but can't be modified", variable = self.q5d)
        self.cb14.grid(row = 20, column = 1, sticky = "w")
        self.s5lbl = Label(self.listFrame, text = "   ")
        self.s5lbl.grid(row = 21, column = 0)

        self.q6lbl = Label(self.listFrame, text = "6. Python checks variable types at compile time")
        self.q6lbl.grid(row = 22, column = 0, sticky = "w")
        self.cb15 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q6a)
        self.cb15.grid(row = 23, column = 0, sticky = "w")
        self.cb16 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q6b)
        self.cb16.grid(row = 24, column = 0, sticky = "w")
        self.s6lbl = Label(self.listFrame, text = "   ")
        self.s6lbl.grid(row = 25, column = 0)

        self.q7lbl = Label(self.listFrame, text = "7. Python has built-in functions")
        self.q7lbl.grid(row = 26, column = 0, sticky = "w")
        self.cb17 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q7a)
        self.cb17.grid(row = 27, column = 0, sticky = "w")
        self.cb18 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q7b)
        self.cb18.grid(row = 28, column = 0, sticky = "w")
        self.s7lbl = Label(self.listFrame, text = "   ")
        self.s7lbl.grid(row = 29, column = 0)

        self.q8lbl = Label(self.listFrame, text = "8. A Tuple cannot contain other data types")
        self.q8lbl.grid(row = 30, column = 0, sticky = "w")
        self.cb19 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q8a)
        self.cb19.grid(row = 31, column = 0, sticky = "w")
        self.cb20 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q8b)
        self.cb20.grid(row = 32, column = 0, sticky = "w")
        self.s8lbl = Label(self.listFrame, text = "   ")
        self.s8lbl.grid(row = 33, column = 0)

        self.q9lbl = Label(self.listFrame, text = "9. A List cannont contain other data types ")
        self.q9lbl.grid(row = 34, column = 0, sticky = "w")
        self.cb21 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q9a)
        self.cb21.grid(row = 35, column = 0, sticky = "w")
        self.cb22 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q9b)
        self.cb22.grid(row = 36, column = 0, sticky = "w")
        self.s9lbl = Label(self.listFrame, text = "   ")
        self.s9lbl.grid(row = 37, column = 0)

        self.q10lbl = Label(self.listFrame, text = "10. Python code can be run anywhere because it is cross-platform")
        self.q10lbl.grid(row = 38, column = 0, sticky = "w")
        self.cb23 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q10a)
        self.cb23.grid(row = 39, column = 0, sticky = "w")
        self.cb24 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q10b)
        self.cb24.grid(row = 40, column = 0, sticky = "w")
        self.s10lbl = Label(self.listFrame, text = "   ")
        self.s10lbl.grid(row = 41, column = 0)

        self.frameThree = Frame(self.master)
        self.frameThree.grid(row = 3 ,column = 0)
        self.grade_button = Button(self.frameThree, text = "Grade", command = self.grade_quiz1)
        self.grade_button.grid(row = 5, column = 0, sticky = "w")
        self.quit_button = Button(self.frameThree, text = "Quit", command = self.master.destroy)
        self.quit_button.grid(row = 5, column = 1, sticky = "w")
        self.lbl = Label(self.frameThree, text = "")
        self.lbl.grid(row = 6, column = 0)


    def questionScrollFunction(self, event):
        # The scrollregion option limits scrolling operations for the canvas...otherwise, it would keep growing.
        self.canvas.configure(scrollregion = self.canvas.bbox("all"), width = 900, height = 600)


    def grade_quiz1(self):
        if self.flag:
            if (self.q1a.get() == 1 and self.q1b.get() == 0):
                self.q1LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q1LblAns.grid(row = 1, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q1LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q1LblAns.grid(row = 1, column = 1, sticky = "w")

            if (self.q2a.get() == 1 and self.q2b.get() == 0):
                self.q2LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q2LblAns.grid(row = 5, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q2LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q2LblAns.grid(row = 5, column = 1, sticky = "w")

            if (self.q3a.get() == 0 and self.q3b.get() == 1 and self.q3c.get() == 1 and self.q3d.get() == 0):
                self.q3LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q3LblAns.grid(row = 9, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q3LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q3LblAns.grid(row = 9, column = 1, sticky = "w")

            if (self.q4a.get() == 1 and self.q4b.get() == 0):
                self.q4LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q4LblAns.grid(row = 14, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q4LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q4LblAns.grid(row = 14, column = 1, sticky = "w")

            if (self.q5a.get() == 0 and self.q5b.get() == 0 and self.q5c.get() == 1 and self.q5d.get() == 1):
                self.q5LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q5LblAns.grid(row = 18, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q5LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q5LblAns.grid(row = 18, column = 1, sticky = "w")

            if (self.q6a.get() == 0 and self.q6b.get() == 1):
                self.q6LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q6LblAns.grid(row = 22, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q6LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q6LblAns.grid(row = 22, column = 1, sticky = "w")

            if (self.q7a.get() == 1 and self.q7b.get() == 0):
                self.q7LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q7LblAns.grid(row = 26, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q7LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q7LblAns.grid(row = 26, column = 1, sticky = "w")

            if (self.q8a.get() == 0 and self.q8b.get() == 1):
                self.q8LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q8LblAns.grid(row = 30, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q8LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q8LblAns.grid(row = 30, column = 1, sticky = "w")

            if (self.q9a.get() == 0 and self.q9b.get() == 1):
                self.q9LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q9LblAns.grid(row = 34, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q9LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q9LblAns.grid(row = 34, column = 1, sticky = "w")

            if (self.q10a.get() == 1 and self.q10b.get() == 0):
                self.q10LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q10LblAns.grid(row = 38, column = 1, sticky = "w")
                self.count += 1
            else:
                self.q10LblAns = Label(self.listFrame, text = "Incorrect", foreground = "red")
                self.q10LblAns.grid(row = 38, column = 1, sticky = "w")


            percentage = self.count/10*100
            if percentage >= 70:
                color = "green"
            else:
                color = "red"
            self.totlbl = Label(self.frameThree, text = "Total Correct: " + str(self.count), background = "white", foreground = color, font = "Times 20", relief = "groove")
            self.totlbl.grid(row = 3, column = 0)
            self.pctlbl = Label(self.frameThree, text = "Percentage: " + str(percentage) + "%", background = "white", foreground = color, font = "Times 20", relief = "groove")
            self.pctlbl.grid(row = 4, column = 0)
            self.space = Label(self.frameThree, text = " ")
            self.space.grid(row = 5, column = 0)
            self.flag = False # Only want to allow grading one time to prevent cheating!
        else:
            self.completelbl = Label(self.frameThree, text = "You can only grade the quiz once!", background = "white", foreground = "red", font = "Times 25", relief = "groove")
            self.completelbl.grid(row = 7, column = 0)
            self.spacey = Label(self.frameThree, text = " ")
            self.spacey.grid(row = 2, column = 0)

    def initUIquiz2(self):
        # ansDict is a dictionary that contains the correct responses to questions
        self.quizFramework(2)
        # Add the questions to listFrame which will be scrollable as needed.
        self.q1Lbl = Label(self.listFrame, text = "1. A list can be changed but a tuple is immutable.")
        self.q1Lbl.grid(row = self.rowCount, column = 0, padx=(10,0), sticky = "w")
        self.displayRowCount[1] = self.rowCount
        self.rowCount += 1
        self.cb1 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q1a)
        self.cb1.grid(row = self.rowCount, column=0, padx=(10,0), sticky = "w")
        self.rowCount += 1
        self.cb2 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q1b)
        self.cb2.grid(row = self.rowCount, column=0, padx=(10,0), sticky = "w")
        self.rowCount += 1
        self.s1lbl = Label(self.listFrame, text = " ")
        self.s1lbl.grid(row = self.rowCount, column = 0)
        self.ansDict[1] = "a"
        self.rowCount += 1
        
        self.q2lbl = Label(self.listFrame, text = "2. Assume myList = ['cat', 'dog', mouse']; what is myList[2][2]?")
        self.q2lbl.grid(row = self.rowCount, column=0, padx=(10,0), sticky = "w")
        self.displayRowCount[2] = self.rowCount
        self.rowCount += 1
        self.cb3 = Checkbutton(self.listFrame, text = "a.  'mouse'", variable = self.q2a)
        self.cb3.grid(row = self.rowCount, column=0, padx=(10,0), sticky = "w")
        self.rowCount += 1
        self.cb4 = Checkbutton(self.listFrame, text = "b.  'u'", variable = self.q2b)
        self.cb4.grid(row = self.rowCount, column=0, padx=(10,0), sticky = "w")
        self.rowCount += 1
        self.s2lbl = Label(self.listFrame, text = "   ")
        self.s2lbl.grid(row = self.rowCount, column = 0)
        self.ansDict[2] = "b"
        self.rowCount += 1
        
        self.q3lbl = Label(self.listFrame, text = "3. Given l = ['z', 'd', 'a', 'v', 'e', 'i', 'o']. What is l[1:5]?")
        self.q3lbl.grid(row = self.rowCount, column = 0, padx = (10,0), sticky = "w")
        self.displayRowCount[3] = self.rowCount
        self.rowCount += 1
        self.cb5 = Checkbutton(self.listFrame, text = "a.  ['z', 'd', 'a', 'v']", variable = self.q3a)
        self.cb5.grid(row = self.rowCount, column = 0, padx = (10,0), sticky = "w")
        self.cb6 = Checkbutton(self.listFrame, text = "b.  ['d', 'a', 'v']", variable = self.q3b)
        self.cb6.grid(row = self.rowCount, column = 1, padx = (10,0), sticky = "w")
        self.rowCount += 1
        self.cb7 = Checkbutton(self.listFrame, text = "c.  ['d', 'a', 'v', 'e']", variable = self.q3c)
        self.cb7.grid(row = self.rowCount, column = 0, padx = (10,0), sticky = "w")
        self.cb8 = Checkbutton(self.listFrame, text = "d.  None of the above", variable = self.q3d)
        self.cb8.grid(row = self.rowCount, column = 1, padx = (10,0), sticky = "w")
        self.rowCount += 1
        self.s3lbl = Label(self.listFrame, text = "   ")
        self.s3lbl.grid(row = self.rowCount, column = 0)
        self.ansDict[3] = "c"
        self.rowCount += 1
                
        self.q4lbl = Label(self.listFrame, text = "4. Given x = (1, 2, 3, 4) and a, *b, c = x then b = 2.")
        self.q4lbl.grid(row = self.rowCount, column = 0, sticky = "w")
        self.displayRowCount[4] = self.rowCount
        self.rowCount += 1
        self.cb9 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q4a)
        self.cb9.grid(row = self.rowCount, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb10 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q4b)
        self.cb10.grid(row = self.rowCount, column = 0, sticky = "w")
        self.rowCount += 1
        self.s4lbl = Label(self.listFrame, text = "   ")
        self.s4lbl.grid(row = self.rowCount, column = 0)
        self.ansDict[4] = "b"
        self.rowCount += 1
         
        self.q5lbl = Label(self.listFrame, text = "5. Which of the following are valid list operations? Check all true responses.")
        self.q5lbl.grid(row = self.rowCount, column = 0, sticky = "w")
        self.displayRowCount[5] = self.rowCount
        self.rowCount += 1
        self.cb11 = Checkbutton(self.listFrame, text = "a.  lenth - Returns the length of a list", variable = self.q5a)
        self.cb11.grid(row = self.rowCount, column = 0, sticky = "w")
        self.cb12 = Checkbutton(self.listFrame, text = "b.  append - Adds a single element to the end of a list", variable = self.q5b)
        self.cb12.grid(row = self.rowCount, column = 1, sticky = "w")
        self.rowCount += 1
        self.cb13 = Checkbutton(self.listFrame, text = "c.  insert - Inserts a new element at a given position in the list", variable = self.q5c)
        self.cb13.grid(row = self.rowCount, column = 0, sticky = "w")
        self.cb14 = Checkbutton(self.listFrame, text = "d.  delete - Removes a list element or slice", variable = self.q5d)
        self.cb14.grid(row = self.rowCount, column = 1, sticky = "w")
        self.rowCount += 1
        self.s5lbl = Label(self.listFrame, text = "   ")
        self.s5lbl.grid(row = self.rowCount, column = 0)
        self.ansDict[5] = "b, c"
        self.rowCount += 1
        
        self.displayRowCount[6] = self.rowCount
        self.q6lbl = Label(self.listFrame, text = "6. A Python tuple with one element requires a comma - e.g. myOneTuple = (1,).")
        self.q6lbl.grid(row = self.rowCount, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb15 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q6a)
        self.cb15.grid(row = self.rowCount, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb16 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q6b)
        self.cb16.grid(row = self.rowCount, column = 0, sticky = "w")
        self.rowCount += 1
        self.s6lbl = Label(self.listFrame, text = "   ")
        self.s6lbl.grid(row = self.rowCount, column = 0)
        self.ansDict[6] = "a"
        self.rowCount += 1

        self.displayRowCount[7] = self.rowCount
        self.q7lbl = Label(self.listFrame, text = "7. The expressions t3 = t1 + t1 and t3 = 2 * t1 are equivalent given t1 is a tuple.")
        self.q7lbl.grid(row = 26, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb17 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q7a)
        self.cb17.grid(row = 27, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb18 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q7b)
        self.cb18.grid(row = 28, column = 0, sticky = "w")
        self.rowCount += 1
        self.s7lbl = Label(self.listFrame, text = "   ")
        self.s7lbl.grid(row = 29, column = 0)
        self.ansDict[7] = "a"
        self.rowCount += 1
        
        self.displayRowCount[8] = self.rowCount
        self.q8lbl = Label(self.listFrame, text = "8. A Tuple can contain other data types.")
        self.q8lbl.grid(row = 30, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb19 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q8a)
        self.cb19.grid(row = 31, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb20 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q8b)
        self.cb20.grid(row = 32, column = 0, sticky = "w")
        self.rowCount += 1
        self.s8lbl = Label(self.listFrame, text = "   ")
        self.s8lbl.grid(row = 33, column = 0)
        self.ansDict[8] = "a"
        self.rowCount += 1
        
        self.displayRowCount[9] = self.rowCount
        self.q9lbl = Label(self.listFrame, text = "9. A set in Python is an ordered collection of objects used when uniqueness is not important.")
        self.q9lbl.grid(row = 34, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb21 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q9a)
        self.cb21.grid(row = 35, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb22 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q9b)
        self.cb22.grid(row = 36, column = 0, sticky = "w")
        self.rowCount += 1
        self.s9lbl = Label(self.listFrame, text = "   ")
        self.s9lbl.grid(row = 37, column = 0)
        self.ansDict[9] = "b"
        self.rowCount += 1
        
        self.displayRowCount[10] = self.rowCount
        self.q10lbl = Label(self.listFrame, text = "10. A set can be used as a dictionary key because it is both immutable and hasbable.")
        self.q10lbl.grid(row = 38, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb23 = Checkbutton(self.listFrame, text = "a.  True", variable = self.q10a)
        self.cb23.grid(row = 39, column = 0, sticky = "w")
        self.rowCount += 1
        self.cb24 = Checkbutton(self.listFrame, text = "b.  False", variable = self.q10b)
        self.cb24.grid(row = 40, column = 0, sticky = "w")
        self.rowCount += 1
        self.s10lbl = Label(self.listFrame, text = "   ")
        self.s10lbl.grid(row = 41, column = 0)
        self.ansDict[10] = "b"
        self.rowCount += 1
        
        self.frameThree = Frame(self.master)
        self.frameThree.grid(row = 2, column = 0)
        self.grade_button = Button(self.frameThree, text = "Grade", command = self.grade_quiz2)
        self.grade_button.grid(row = 5, column = 0, sticky = "w")
        self.quit_button = Button(self.frameThree, text = "Quit", command = self.master.destroy)
        self.quit_button.grid(row = 5, column = 1, sticky = "w")
        self.lbl = Label(self.frameThree, text = "")
        self.lbl.grid(row = 6, column = 0)

    def grade_quiz2(self):
        if self.flag:
            if (self.q1a.get() == 1 and self.q1b.get() == 0):
                self.q1LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q1LblAns.grid(row = self.displayRowCount[1], column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[1] + ")"
                self.q1LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q1LblAns.grid(row = self.displayRowCount[1], column = 1, sticky = "w")

            if (self.q2a.get() == 0 and self.q2b.get() == 1):
                self.q2LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q2LblAns.grid(row = self.displayRowCount[2], column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[2] + ")"
                self.q2LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q2LblAns.grid(row = self.displayRowCount[2], column = 1, sticky = "w")

            if (self.q3a.get() == 0 and self.q3b.get() == 0 and self.q3c.get() == 1 and self.q3d.get() == 0):
                self.q3LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q3LblAns.grid(row = self.displayRowCount[3], column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[3] + ")"
                self.q3LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q3LblAns.grid(row = self.displayRowCount[3], column = 1, sticky = "w")

            if (self.q4a.get() == 0 and self.q4b.get() == 1):
                self.q4LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q4LblAns.grid(row = self.displayRowCount[4], column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[4] + ")"
                self.q4LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q4LblAns.grid(row = self.displayRowCount[4], column = 1, sticky = "w")

            if (self.q5a.get() == 0 and self.q5b.get() == 1 and self.q5c.get() == 1 and self.q5d.get() == 0):
                self.q5LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q5LblAns.grid(row = self.displayRowCount[5], column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[5] + ")"
                self.q5LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q5LblAns.grid(row = self.displayRowCount[5], column = 1, sticky = "w")

            if (self.q6a.get() == 1 and self.q6b.get() == 0):
                self.q6LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q6LblAns.grid(row = 22, column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[6] + ")"
                self.q6LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q6LblAns.grid(row = 22, column = 1, sticky = "w")

            if (self.q7a.get() == 1 and self.q7b.get() == 0):
                self.q7LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q7LblAns.grid(row = 26, column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[7] + ")"
                self.q7LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q7LblAns.grid(row = 26, column = 1, sticky = "w")

            if (self.q8a.get() == 1 and self.q8b.get() == 0):
                self.q8LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q8LblAns.grid(row = 30, column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[8] + ")"
                self.q8LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q8LblAns.grid(row = 30, column = 1, sticky = "w")

            if (self.q9a.get() == 0 and self.q9b.get() == 1):
                self.q9LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q9LblAns.grid(row = 34, column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[9] + ")"
                self.q9LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q9LblAns.grid(row = 34, column = 1, sticky = "w")

            if (self.q10a.get() == 0 and self.q10b.get() == 1):
                self.q10LblAns = Label(self.listFrame, text = "Correct  ", foreground = "green")
                self.q10LblAns.grid(row = 38, column = 1, sticky = "w")
                self.count += 1
            else:
                displayText = "Incorrect" + "   (" + self.ansDict[10] + ")"
                self.q10LblAns = Label(self.listFrame, text = displayText, foreground = "red")
                self.q10LblAns.grid(row = 38, column = 1, sticky = "w")


            percentage = self.count/10*100
            if percentage >= 70:
                color = "green"
            else:
                color = "red"
            self.totlbl = Label(self.frameThree, text = "Total Correct: " + str(self.count), background = "white", foreground = color, font = "Times 20", relief = "groove")
            self.totlbl.grid(row = 3, column = 0)
            self.pctlbl = Label(self.frameThree, text = "Percentage: " + str(percentage) + "%", background = "white", foreground = color, font = "Times 20", relief = "groove")
            self.pctlbl.grid(row = 4, column = 0)
            self.space = Label(self.frameThree, text = " ")
            self.space.grid(row = 5, column = 0)
            self.flag = False # Only want to allow grading one time to prevent cheating!
        else:
            self.completelbl = Label(self.frameThree, text = "You can only grade the quiz once!", background = "white", foreground = "red", font = "Times 25", relief = "groove")
            self.completelbl.grid(row = 7, column = 0)
            self.spacey = Label(self.frameThree, text = " ")
            self.spacey.grid(row = 2, column = 0)

if __name__ == "__main__":
    root = Tk()
    try:
        quiz = Quiz(root)
    except:
        print("Caught an exception but moving on.")
    root.mainloop()
    
